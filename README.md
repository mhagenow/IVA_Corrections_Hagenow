# Affordance Templates with Human-in-the-loop Corrections

![teaser](/media/fabricated.png)

This code provides methods to provide small corrections when attempting to register affordances (e.g., meshes, primitives) in
a 3D environment. Specifically, the method leverages Iterative Closest Points (ICP) with human augmentation to help escape from
local minima. The code is also able to fit/correct models including ariculations (e.g., valves) using a nonlinear optimization within the ICP.

The majority of the relevant code is contained within the affordance_corrections package in the src directory.

## Dependencies/Requirements
Required:
- ROS (melodic is required for the Azure drivers -- noetic is compatible with the rest of the code)
- Catkin build (`sudo apt-get install ros-melodic-catkin python3-catkin-tools python3-osrf-pycommon`)
- Trimesh (`pip3 install trimesh`)
- Open3D (`pip3 install open3d`)
- Qt5 (`sudo apt-get install qt5-default`)
- Numpy
- Scipy

Required for C++ Fitting:
- Eigen (Version 3.4+). We recommend building and installing from source (https://gitlab.com/libeigen/eigen)
- OpenMP (`sudo apt-get install libomp-dev`)
- NLOpt (https://nlopt.readthedocs.io/en/latest/#download-and-installation)
- PicoTree (https://github.com/Jaybro/pico_tree)

Optional:
- PCL ROS (`sudo apt-get install ros-melodic-pcl-ros`) (only needed for converting point cloud utilities)
- Rtree (`pip3 install Rtree`) (only needed to center STLs around the centroid)
- Spacemouse Spacenav (`sudo apt install spacenavd; sudo apt install ros-melodic-spacenav-node`) (only needed for spacemouse input)
- Microsoft Azure SDK (can be installed from a [Debian package](https://github.com/microsoft/Azure-Kinect-Sensor-SDK/blob/develop/docs/usage.md) instead of building from src) (only needed to stream azure point cloud)

## Demo Application ##
The main application leverages a 6D input (e.g., spacemouse, vive) to provide the corrections. However, to allow for portability and use
without such devices, there is a demo that allows you to try the system using interactive markers.

To run the demo:
1. run `./compile.bash` from the root directory.
2. source the ROS workspace (`source devel/setup.bash`)
3. run `roslaunch affordance_corrections marker_specification.launch` 
4. This will pull up the interface and a scene where you can try the fitting with corrections. You can select points by either pressing 'c' or clicking 'publish point'. After a point is selected, the system will fit the catalog of models and select the best fit. You can then update incorrect fits using the interactive markers. You can use a slider on the left to update the articulation if the model has a joint. You can also 'c' click on gray (inactive objects) to get focus, and clicking again on the active object will cycle to the next most likely model. You can also see the sorted models by right-clicking on the interactive markers.
5. To run the cppfitting, run `./cppcompile.bash` from the root directory. If the library builds successfully, you can enable the fitting at the bottom of `src/affordance_corrections/nodes/affordance_corrections/ros_marker_wrapper.py` by setting `rosmarkaff.setCppFitting(True)` 

## Diagram of Codebase
![codebase](/media/code_diagram.png)

