# Taskboard Design

![task board](../media/taskboard.png)

### Bill of Materials (BOM)
Designed with five objects currently:
1. Stop Valve (https://www.amazon.com/dp/B00MA9EZ14?psc=1&ref=ppx_yo2_dt_b_product_details)
2. PVC Ball Valve (https://www.amazon.com/dp/B0046HAAYY?psc=1&ref=ppx_yo2_dt_b_product_details)
3. Ball Valve (https://www.amazon.com/dp/B00PILUMQS?psc=1&ref=ppx_yo2_dt_b_product_details)
4. Drawer Handle (Home depot)
5. E-Stop (Digikey)

### Board
The board was laser cut out of 1/4" HDF. The PDF used for laser cutting is available in this directory. The design is 18x24".

### Assembling
The pieces are attached to the board using M6 Hardware. A variety of 3D printed interfaces for holding the hardware and for mounting the board are available in the '3d_printing_holders' directory.

