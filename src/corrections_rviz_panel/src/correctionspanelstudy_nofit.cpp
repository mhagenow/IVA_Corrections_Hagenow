// File name:  correctionspanel.cpp
// Description: implementation related to custom rviz panel for visual information and publishing
// Author: Mike Hagenow
// Date: 6/21/21
// Using information found in the ROS
// custom plugins tutorial: https://github.com/ros-visualization/visualization_tutorials/blob/groovy-devel/rviz_plugin_tutorials/src/teleop_panel.cpp

#include <stdio.h>
#include "correctionspanelstudy_nofit.h"

#include <QWidget>
#include <QPainter>
#include <QLabel>
#include <QPushButton>
#include <QTimer>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFont>
#include <QSizePolicy>


#include <ros/console.h>

#include <rviz/visualization_manager.h>
#include <rviz/view_controller.h>
#include <rviz/render_panel.h>
#include <rviz/view_manager.h>
#include <OGRE/OgreCamera.h>
#include <OgreQuaternion.h>
#include <OgreVector3.h>

#include "drive_widget.h"

namespace correction_panel{

    CorrectionPanelStudyNofit::CorrectionPanelStudyNofit(QWidget* parent): rviz::Panel(parent){

        // Initialize publishers
        cam_pos_pub = n.advertise<geometry_msgs::Point>("rviz_camera_p", 1);
        quat_pub = n.advertise<geometry_msgs::Quaternion>("rviz_camera_q", 1);
        trigger_pub = n.advertise<std_msgs::String>("rviz_triggers", 1);
        artic_pub = n.advertise<std_msgs::Int8>("/rviz/artic", 1);

        // Constructor sets up the graphical object
        trial_running = false;
        QPushButton* toggleTrialbutton = new QPushButton("Start Trial");
        QPushButton* deletebutton = new QPushButton("Delete Active Object");
        toggleTrialbutton->setStyleSheet("background-color: #B6D5E7; border-style: solid; border-width: 2px; border-radius: 10px; border-color: #B6D5E7; font: bold 22px; min-width: 10em; padding: 6px;");
        deletebutton->setStyleSheet("background-color: #FF968A; border-style: solid; border-width: 2px; border-radius: 10px; border-color: #FF968A; font: bold 22px; min-width: 10em; padding: 6px;");

        // QPushButton* flipbutton = new QPushButton("Flip Fit");
        QVBoxLayout* vlayout = new QVBoxLayout;

        QWidget *studyBox = new QWidget;
        QVBoxLayout* studyLayout = new QVBoxLayout(studyBox);
        studyBox->setStyleSheet("background-color: #dae3e3; border-radius: 10px; border-color: #b6b8b8");
        studyBox->setFixedHeight(200);

  
        //  Title has larger bold font
        QLabel* title = new QLabel("Registration Study");
        // title->setStyleSheet("border-style: outset; border-width: 2px; text-align: center;");
        title->setAlignment(Qt::AlignCenter);
        title->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Fixed);
        QLabel* title2 = new QLabel("SpaceMouse");
        title2->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Fixed);
        title2->setAlignment(Qt::AlignCenter);
        // title2->setStyleSheet("border-style: outset; border-width: 2px; text-align: center;");
        QFont title_font = title -> font();
        title_font.setPointSize(30);
        title_font.setBold(true);
        title->setFont(title_font);
        QFont title2_font = title -> font();
        title2_font.setPointSize(20);
        title2_font.setBold(true);
        title2->setFont(title2_font);
        studyLayout->addWidget(title);
        studyLayout->addWidget(title2);
        studyLayout->addWidget(toggleTrialbutton);

        vlayout->addWidget(studyBox);
        vlayout->addWidget(deletebutton);

        vlayout->setSpacing(0);
        setLayout(vlayout);

        // Start trial sends a trigger
        connect(toggleTrialbutton, &QPushButton::clicked, [this,toggleTrialbutton](){
           if(!trial_running){
            s_out.data = "starttrial";
            toggleTrialbutton->setText("End Trial");
            toggleTrialbutton->setStyleSheet("background-color: #FF968A; border-style: solid; border-width: 2px; border-radius: 10px; border-color: #B6D5E7; font: bold 22px; min-width: 10em; padding: 6px;");
            trial_running = true;
           }
           else{
               s_out.data = "endtrial";
               toggleTrialbutton->setText("Start Trial");
               toggleTrialbutton->setStyleSheet("background-color: #B6D5E7; border-style: solid; border-width: 2px; border-radius: 10px; border-color: #B6D5E7; font: bold 22px; min-width: 10em; padding: 6px;");
               trial_running = false;
           }
           trigger_pub.publish(s_out);
        });

        // Delete button sends a trigger
        connect(deletebutton, &QPushButton::clicked, [this](){
           s_out.data = "deleteactive";
           trigger_pub.publish(s_out);
        });
        
        // Timer used to publish the camera orientation from RVIZ for camera-centric controls
        QTimer* output_timer = new QTimer( this );  
        connect(output_timer, &QTimer::timeout, [this](){
            rviz::ViewManager* viewm = vis_manager_->getViewManager();
            rviz::ViewController* vc = viewm->getCurrent();
            Ogre::Camera* camera = vc->getCamera();
            const Ogre::Quaternion quat = camera->getOrientation();
            const Ogre::Vector3 cam_pos = camera->getPosition();
            
            // Convert from Ogre to ROS message
            q_out.x = quat.x; q_out.y = quat.y; q_out.z = quat.z; q_out.w = quat.w;
            pos_out.x = cam_pos.x; pos_out.y = cam_pos.y; pos_out.z = cam_pos.z; 
            quat_pub.publish(q_out);
            cam_pos_pub.publish(pos_out);
        }); 
        output_timer->start(100);

    }

} // end namespace

// Make pluginlib aware of the class
#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(correction_panel::CorrectionPanelStudyNofit,rviz::Panel)
