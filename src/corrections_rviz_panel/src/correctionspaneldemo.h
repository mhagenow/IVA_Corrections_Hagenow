// File name:  correctionspanel.h
// Description: header related to custom rviz panel for visual information and publishing
// Author: Mike Hagenow
// Date: 6/21/21
// Using information found in the ROS
// custom plugins tutorial: https://github.com/ros-visualization/visualization_tutorials/blob/groovy-devel/rviz_plugin_tutorials/src/teleop_panel.cpp

#ifndef CORR_PANEL_DEMO_H
#define CORR_PANEL_DEMO_H

#include <ros/ros.h>
#include <rviz/panel.h>
#include "geometry_msgs/Quaternion.h"
#include "geometry_msgs/Point.h"
#include "std_msgs/String.h"

namespace correction_panel
{
    class CorrectionPanelDemo: public rviz::Panel{
        //Q_OBJECT
        public:
            CorrectionPanelDemo( QWidget* parent = 0);
        private:
            ros::NodeHandle n;
            ros::Publisher quat_pub;
            ros::Publisher cam_pos_pub;
            geometry_msgs::Quaternion q_out;
            geometry_msgs::Point pos_out;
            ros::Publisher trigger_pub;
            ros::Publisher artic_pub;
            ros::Publisher manip_pub;
            std_msgs::String s_out;
            

    };
} // end of namespace
#endif