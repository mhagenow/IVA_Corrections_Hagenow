// File name:  correctionspanel.cpp
// Description: implementation related to custom rviz panel for visual information and publishing
// Author: Mike Hagenow
// Date: 6/21/21
// Using information found in the ROS
// custom plugins tutorial: https://github.com/ros-visualization/visualization_tutorials/blob/groovy-devel/rviz_plugin_tutorials/src/teleop_panel.cpp

#include <stdio.h>
#include "correctionspaneldemo.h"

#include <ros/console.h>

#include <rviz/visualization_manager.h>
#include <rviz/view_controller.h>
#include <rviz/render_panel.h>
#include <rviz/view_manager.h>
#include <OGRE/OgreCamera.h>
#include <OgreQuaternion.h>
#include <OgreVector3.h>

#include <QWidget>
#include <QPainter>
#include <QLabel>
#include <QPushButton>
#include <QTimer>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFont>
#include <QSizePolicy>
#include <QLineEdit>


namespace correction_panel{

    CorrectionPanelDemo::CorrectionPanelDemo(QWidget* parent): rviz::Panel(parent){

        // Initialize publishers
        cam_pos_pub = n.advertise<geometry_msgs::Point>("rviz_camera_p", 1);
        quat_pub = n.advertise<geometry_msgs::Quaternion>("rviz_camera_q", 1);
        trigger_pub = n.advertise<std_msgs::String>("rviz_triggers", 1);
        artic_pub = n.advertise<std_msgs::String>("nextarticulation", 1);
        manip_pub = n.advertise<std_msgs::String>("/panda/executeAT", 1);

        QWidget *studyBox = new QWidget;
        QVBoxLayout* studyLayout = new QVBoxLayout(studyBox);
        studyBox->setStyleSheet("background-color: #dae3e3; border-radius: 10px; border-color: #b6b8b8");
        studyBox->setFixedHeight(100);


        // Constructor sets up the graphical object
        
        QVBoxLayout* vlayout = new QVBoxLayout;
        
        //  Title has larger bold font
       //  Title has larger bold font
        QLabel* title = new QLabel("Situated Example");
        title->setAlignment(Qt::AlignCenter);
        title->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Fixed);
        QFont title_font = title -> font();
        title_font.setPointSize(30);
        title_font.setBold(true);
        title->setFont(title_font);
        studyLayout->addWidget(title);

        QWidget *articBox = new QWidget;
        QVBoxLayout* articLayout = new QVBoxLayout(articBox);
        articBox->setStyleSheet("background-color: #dae3e3; border-radius: 10px; border-color: #b6b8b8");
        articBox->setFixedHeight(150);
        QLabel* maniptitle = new QLabel("Desired Articulation Angle:");
        maniptitle->setAlignment(Qt::AlignCenter);
        maniptitle->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Fixed);
        QFont maniptitle_font = maniptitle -> font();
        maniptitle_font.setPointSize(15);
        maniptitle_font.setBold(false);
        maniptitle->setFont(maniptitle_font);
        QLineEdit* manip_angle = new QLineEdit("");
        QFont manipangle_font = manip_angle -> font();
        manipangle_font.setPointSize(15);
        manipangle_font.setBold(false);
        manip_angle->setFont(manipangle_font);
        manip_angle->setStyleSheet("background-color: #eeeeee; height:30px; border-radius: 5px; border-width: 2px; border-color: #999999");
        manip_angle->setAlignment(Qt::AlignCenter);
        QPushButton* manipbutton = new QPushButton("Perform Manipulation");
        manipbutton->setStyleSheet("background-color: #B6D5E7; border-style: solid; border-width: 2px; border-radius: 10px; border-color: #B6D5E7; font: bold 22px; min-width: 10em; padding: 6px;");
        articLayout->addWidget(maniptitle);
        articLayout->addWidget(manip_angle);
        articLayout->addWidget(manipbutton);

        QPushButton* deletebutton = new QPushButton("Delete Active Object");
        deletebutton->setStyleSheet("background-color: #FF968A; border-style: solid; border-width: 2px; border-radius: 10px; border-color: #FF968A; font: bold 22px; min-width: 10em; padding: 6px;");

        
        // Rest of objects: instructions and 'clear all button
        vlayout->addWidget(studyBox);
        vlayout->addWidget(articBox);
        vlayout->addWidget(deletebutton);
        setLayout(vlayout);
        

        // Sends trigger to ros_affordance_engine
        connect( manipbutton, &QPushButton::clicked, [this,manip_angle](){
           s_out.data = manip_angle->text().toStdString();
           manip_pub.publish(s_out);
        });

        // Sends trigger to ros_affordance_engine to delete active object
        connect( deletebutton, &QPushButton::clicked, [this](){
           s_out.data = "deleteactive";
           trigger_pub.publish(s_out);
        });
        
        // Timer used to publish the camera orientation from RVIZ for camera-centric controls
        QTimer* output_timer = new QTimer( this );  
        connect(output_timer, &QTimer::timeout, [this](){
            rviz::ViewManager* viewm = vis_manager_->getViewManager();
            rviz::ViewController* vc = viewm->getCurrent();
            Ogre::Camera* camera = vc->getCamera();
            const Ogre::Quaternion quat = camera->getOrientation();
            const Ogre::Vector3 cam_pos = camera->getPosition();
            
            // Convert from Ogre to ROS message
            q_out.x = quat.x; q_out.y = quat.y; q_out.z = quat.z; q_out.w = quat.w;
            pos_out.x = cam_pos.x; pos_out.y = cam_pos.y; pos_out.z = cam_pos.z; 
            quat_pub.publish(q_out);
            cam_pos_pub.publish(pos_out);
        }); 
        output_timer->start(100);

    }

   
} // end namespace


// Make pluginlib aware of the class
#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(correction_panel::CorrectionPanelDemo,rviz::Panel)
