// File name:  correctionspanel.cpp
// Description: implementation related to custom rviz panel for visual information and publishing
// Author: Mike Hagenow
// Date: 6/21/21
// Using information found in the ROS
// custom plugins tutorial: https://github.com/ros-visualization/visualization_tutorials/blob/groovy-devel/rviz_plugin_tutorials/src/teleop_panel.cpp

#include <stdio.h>
#include "correctionspanelnoteleop.h"

#include <QPainter>
#include <QLabel>
#include <QPushButton>
#include <QTimer>
#include <QVBoxLayout>
#include <QHBoxLayout>

#include <ros/console.h>

#include <rviz/visualization_manager.h>
#include <rviz/view_controller.h>
#include <rviz/render_panel.h>
#include <rviz/view_manager.h>
#include <OGRE/OgreCamera.h>
#include <OgreQuaternion.h>


namespace correction_panel{

    CorrectionPanelNoTeleop::CorrectionPanelNoTeleop(QWidget* parent): rviz::Panel(parent){

        // Initialize publishers
        cam_pos_pub = n.advertise<geometry_msgs::Point>("rviz_camera_p", 1);
        quat_pub = n.advertise<geometry_msgs::Quaternion>("rviz_camera_q", 1);
        trigger_pub = n.advertise<std_msgs::String>("rviz_triggers", 1);
        artic_pub = n.advertise<std_msgs::String>("nextarticulation", 1);


        // Constructor sets up the graphical object
        QPushButton* button2 = new QPushButton("Cycle Articulation");
        QPushButton* button4 = new QPushButton("Clear Active");
        QPushButton* button = new QPushButton("Clear All");
        QVBoxLayout* vlayout = new QVBoxLayout;
        
        //  Title has larger bold font
        QLabel* title = new QLabel("Affordance Specification");
        QFont title_font = title -> font();
        title_font.setPointSize(20);
        title_font.setBold(true);
        title->setFont(title_font);
        vlayout->addWidget(title);
        
        // Rest of objects: instructions and 'clear all button
        vlayout->addWidget(new QLabel("Press 'c' or use the 'Publish Point' button\nto select points of interest\n-Click a gray object to get focus\n-Click an object again to cycle objects"));
        vlayout->addWidget(button2);
        vlayout->addWidget(button4);
        vlayout->addWidget(button);
        setLayout(vlayout);
        
        
        // Clear all button sends a trigger
        connect( button, &QPushButton::clicked, [this](){
           s_out.data = "clear";
           trigger_pub.publish(s_out);
        });

        // Cycle Articulation button sends a trigger
        connect( button2, &QPushButton::clicked, [this](){
           s_out.data = "artic";
           artic_pub.publish(s_out);
        });

        // Sends trigger to ros_affordance_engine to delete active object
        connect( button4, &QPushButton::clicked, [this](){
           s_out.data = "deleteactive";
           trigger_pub.publish(s_out);
        });
        
        // Timer used to publish the camera orientation from RVIZ for camera-centric controls
        QTimer* output_timer = new QTimer( this );  
        connect(output_timer, &QTimer::timeout, [this](){
            rviz::ViewManager* viewm = vis_manager_->getViewManager();
            rviz::ViewController* vc = viewm->getCurrent();
            Ogre::Camera* camera = vc->getCamera();
            const Ogre::Quaternion quat = camera->getOrientation();
            const Ogre::Vector3 cam_pos = camera->getPosition();
            
            // Convert from Ogre to ROS message
            q_out.x = quat.x; q_out.y = quat.y; q_out.z = quat.z; q_out.w = quat.w;
            pos_out.x = cam_pos.x; pos_out.y = cam_pos.y; pos_out.z = cam_pos.z; 
            quat_pub.publish(q_out);
            cam_pos_pub.publish(pos_out);
        }); 
        output_timer->start(100);

    }

   
} // end namespace


// Make pluginlib aware of the class
#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(correction_panel::CorrectionPanelNoTeleop,rviz::Panel)
