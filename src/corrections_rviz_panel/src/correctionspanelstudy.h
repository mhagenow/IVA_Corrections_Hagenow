// File name:  correctionspanel.h
// Description: header related to custom rviz panel for visual information and publishing
// Author: Mike Hagenow
// Date: 6/21/21
// Using information found in the ROS
// custom plugins tutorial: https://github.com/ros-visualization/visualization_tutorials/blob/groovy-devel/rviz_plugin_tutorials/src/teleop_panel.cpp

#ifndef CORR_PANEL_STUDY_H
#define CORR_PANEL_STUDY_H

#include <ros/ros.h>
#include <rviz/panel.h>
#include "geometry_msgs/Quaternion.h"
#include "geometry_msgs/Point.h"
#include "std_msgs/Int8.h"
#include "std_msgs/String.h"
#include "std_msgs/Bool.h"

#include <QCheckBox>

namespace correction_panel
{
    // forward declaration to avoid "does not name a type" cyclic dependency
    class DriveWidget;

    class CorrectionPanelStudy: public rviz::Panel{
        public:
            CorrectionPanelStudy( QWidget* parent = 0);
            void refitCallback(std_msgs::Bool data);
        protected:
            DriveWidget* drive_widget_;
        private:
            ros::NodeHandle n;
            ros::Publisher quat_pub;
            ros::Publisher cam_pos_pub;
            ros::Subscriber refit_sub;
            geometry_msgs::Quaternion q_out;
            geometry_msgs::Point pos_out;
            ros::Publisher trigger_pub;
            ros::Publisher flip_pub;
            std_msgs::String s_out;

            QCheckBox* refitting;

            std_msgs::Int8 artic_out;
            ros::Publisher artic_pub;
            
            bool trial_running;
            
    };
} // end of namespace
#endif