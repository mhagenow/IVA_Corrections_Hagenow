// File name:  correctionspanel.cpp
// Description: implementation related to custom rviz panel for visual information and publishing
// Author: Mike Hagenow
// Date: 6/21/21
// Using information found in the ROS
// custom plugins tutorial: https://github.com/ros-visualization/visualization_tutorials/blob/groovy-devel/rviz_plugin_tutorials/src/teleop_panel.cpp

#include <stdio.h>
#include "correctionspanelstudy_intnofit.h"

#include <ros/console.h>

#include <rviz/visualization_manager.h>
#include <rviz/view_controller.h>
#include <rviz/render_panel.h>
#include <rviz/view_manager.h>
#include <OGRE/OgreCamera.h>
#include <OgreQuaternion.h>
#include <OgreVector3.h>

#include <QWidget>
#include <QPainter>
#include <QLabel>
#include <QPushButton>
#include <QTimer>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFont>
#include <QSizePolicy>


#include "drive_widget.h"

namespace correction_panel{

    CorrectionPanelStudyIntNofit::CorrectionPanelStudyIntNofit(QWidget* parent): rviz::Panel(parent){

        // Initialize publishers
        cam_pos_pub = n.advertise<geometry_msgs::Point>("rviz_camera_p", 1);
        quat_pub = n.advertise<geometry_msgs::Quaternion>("rviz_camera_q", 1);
        trigger_pub = n.advertise<std_msgs::String>("rviz_triggers", 1);
        artic_pub = n.advertise<std_msgs::Int8>("/rviz/artic", 1);

        // Subscriber for joint angle
        ang_sub = n.subscribe("rviz_triggers", 1, &CorrectionPanelStudyIntNofit::triggerCallback, this);
        // refit_sub = n.subscribe("objRefittingUpdate", 1, &CorrectionPanelStudyInt::refitCallback, this);

        // Constructor sets up the graphical object
        trial_running = false;
        QPushButton* toggleTrialbutton = new QPushButton("Start Trial");
        // refitting = new QCheckBox("Refitting Enabled");
        // refitting->setChecked(true);
        // refitting->setStyleSheet("font: bold 18px; min-width: 10em; padding-left:85%; margin-left:50%; margin-right:50%;"); 
        QPushButton* deletebutton = new QPushButton("Delete Active Object");
        toggleTrialbutton->setStyleSheet("background-color: #B6D5E7; border-style: solid; border-width: 2px; border-radius: 10px; border-color: #B6D5E7; font: bold 22px; min-width: 10em; padding: 6px;");
        deletebutton->setStyleSheet("background-color: #FF968A; border-style: solid; border-width: 2px; border-radius: 10px; border-color: #FF968A; font: bold 22px; min-width: 10em; padding: 6px;");

        // QPushButton* flipbutton = new QPushButton("Flip Fit");
        QVBoxLayout* vlayout = new QVBoxLayout;

        QWidget *studyBox = new QWidget;
        QVBoxLayout* studyLayout = new QVBoxLayout(studyBox);
        studyBox->setStyleSheet("background-color: #dae3e3; border-radius: 10px; border-color: #b6b8b8");
        studyBox->setFixedHeight(250);

        QWidget *articBox = new QWidget;
        QVBoxLayout* articLayout = new QVBoxLayout(articBox);
        articBox->setStyleSheet("background-color: #dae3e3; border-radius: 10px; border-color: #b6b8b8");
        articBox->setFixedHeight(150);
        jointSlider = new QSlider(Qt::Horizontal);
        jointSlider->setStyleSheet("QSlider::groove:horizontal {background-color: #eeeeee; height: 35px;} QSlider::handle:horizontal {background-color: #726080; width: 35px; height: 35px; border-radius:10px;}");
        jointSlider->setTracking(true);
        jointSlider->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum);
        QLabel* slidertitle = new QLabel("Adjust Articulation");
        QFont slidertitle_font = slidertitle -> font();
        slidertitle_font.setPointSize(20);
        slidertitle_font.setBold(true);
        slidertitle->setFont(slidertitle_font);
        slidertitle->setAlignment(Qt::AlignCenter);
        slidertitle->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Fixed);
        articLayout->addWidget(slidertitle);
        articLayout->addWidget(jointSlider);
        // sliderlayout->setStyleSheet("border: 1px solid gray;");
        
        //  Title has larger bold font
        QLabel* title = new QLabel("Registration Study");
        // title->setStyleSheet("border-style: outset; border-width: 2px; text-align: center;");
        title->setAlignment(Qt::AlignCenter);
        title->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Fixed);
        QLabel* title2 = new QLabel("Interactive Markers");
        title2->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Fixed);
        title2->setAlignment(Qt::AlignCenter);
        // title2->setStyleSheet("border-style: outset; border-width: 2px; text-align: center;");
        QFont title_font = title -> font();
        title_font.setPointSize(30);
        title_font.setBold(true);
        title->setFont(title_font);
        QFont title2_font = title -> font();
        title2_font.setPointSize(20);
        title2_font.setBold(true);
        title2->setFont(title2_font);
        studyLayout->addWidget(title);
        studyLayout->addWidget(title2);
        studyLayout->addWidget(toggleTrialbutton);
        // studyLayout->addWidget(refitting);

        vlayout->addWidget(studyBox);
        vlayout->addWidget(articBox);
        vlayout->addWidget(deletebutton);
        setLayout(vlayout);

        // Start trial sends a trigger
        connect(toggleTrialbutton, &QPushButton::clicked, [this,toggleTrialbutton](){
           if(!trial_running){
            s_out.data = "starttrial";
            toggleTrialbutton->setText("End Trial");
            toggleTrialbutton->setStyleSheet("background-color: #FF968A; border-style: solid; border-width: 2px; border-radius: 10px; border-color: #B6D5E7; font: bold 22px; min-width: 10em; padding: 6px;");
            trial_running = true;
           }
           else{
               s_out.data = "endtrial";
               toggleTrialbutton->setText("Start Trial");
               toggleTrialbutton->setStyleSheet("background-color: #B6D5E7; border-style: solid; border-width: 2px; border-radius: 10px; border-color: #B6D5E7; font: bold 22px; min-width: 10em; padding: 6px;");
               trial_running = false;
           }
           trigger_pub.publish(s_out);
        });

        // As joint slider is updating, send new joint values
        connect(jointSlider, &QSlider::sliderMoved, [this](){
           int val_temp = jointSlider->value();
           s_out.data = "angle:"+std::to_string(val_temp);
           trigger_pub.publish(s_out);
        });

        connect(deletebutton, &QPushButton::clicked, [this](){
           s_out.data = "deleteactive";
           trigger_pub.publish(s_out);
        });

        // connect(refitting, &QCheckBox::stateChanged, [this](){
        //    if(refitting->isChecked()){
        //        s_out.data = "refittingon";
        //    }
        //    else{
        //        s_out.data = "refittingoff";
        //    }
           
        //    trigger_pub.publish(s_out);
        // });
        
        // Timer used to publish the camera orientation from RVIZ for camera-centric controls
        QTimer* output_timer = new QTimer( this );  
        connect(output_timer, &QTimer::timeout, [this](){
            rviz::ViewManager* viewm = vis_manager_->getViewManager();
            rviz::ViewController* vc = viewm->getCurrent();
            Ogre::Camera* camera = vc->getCamera();
            const Ogre::Quaternion quat = camera->getOrientation();
            const Ogre::Vector3 cam_pos = camera->getPosition();
            
            // Convert from Ogre to ROS message
            q_out.x = quat.x; q_out.y = quat.y; q_out.z = quat.z; q_out.w = quat.w;
            pos_out.x = cam_pos.x; pos_out.y = cam_pos.y; pos_out.z = cam_pos.z; 
            quat_pub.publish(q_out);
            cam_pos_pub.publish(pos_out);

            ros::spinOnce();
        }); 
        output_timer->start(100);

    }

    void CorrectionPanelStudyIntNofit::triggerCallback(std_msgs::String data){
        // Update the slider position of the trigger is telling rviz there is an updated angle
        if(data.data.find("anglerviz")!=std::string::npos){
            std::string temp = data.data.substr(data.data.find(":")+1,data.data.length());
            jointSlider->setSliderPosition(stoi(temp));
        } 
    }

    // void CorrectionPanelStudyIntNofit::refitCallback(std_msgs::Bool data){
    //     // Update the refitting checkbox for the active object
    //     refitting->setChecked(data.data);
    // }

} // end namespace

// Make pluginlib aware of the class
#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(correction_panel::CorrectionPanelStudyIntNofit,rviz::Panel)
