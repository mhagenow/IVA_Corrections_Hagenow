#!/bin/bash

# Mike Hagenow
# 6/18/21
# Compile script for building all files
# mainly using Catkin build utility

catkin build affordance_corrections --no-notify
catkin build affordance_models --no-notify
catkin build corrections_rviz_panel --no-notify

# kinect only builds against melodic (currently)
FILE=/opt/ros/melodic
if [ -d "$FILE" ]; then
    catkin build Azure_Kinect_ROS_Driver-melodic --no-notify
fi
